var mongoose = require('mongoose');  
var projectSchema = new mongoose.Schema({ 
  naziv: String,
  opis: String,
  cijena: Number,
  obavljeni_poslovi: {type: Array, default: []},
  pocetak: {type: Date, default: Date.now},
  zavrsetak: {type: Date, default: Date.now},
  clanovi: {type: Array, default: []},
  voditelj: String,
  arhiviran: Boolean
});
mongoose.model('Project', projectSchema);