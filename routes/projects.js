var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}))

router.route('/')
    .get(function(req, res, next) {
    mongoose.model('Project').find({})
    .then((projects) => {
        res.format({
          html: function(){
              res.render('projects/index', {
                    title: 'All Projects',
                    "projects" : projects
                });
          },
          json: function(){
              res.json(projects);
          }
        });
    })
    .catch((err) => {
        return console.error("Error while listing data: "+ err);
    }) 
})

    .post(function(req, res) {
        var naziv = req.body.naziv;
        var opis = req.body.opis;
        var cijena = req.body.cijena;
        var pocetak = req.body.pocetak;
        var zavrsetak = req.body.zavrsetak;
        var voditelj = req.body.voditelj;
        var arhiviran = req.body.arhiviran;
        if (arhiviran == 'on'){
            arhiviran = true;
        }
        else {
            arhiviran = false;
        }
        mongoose.model('Project').create({
            naziv : naziv,
            opis : opis,
            cijena : cijena,
            pocetak : pocetak,
            zavrsetak: zavrsetak,
            voditelj: voditelj,
            arhiviran: arhiviran
        })
        .then((project) => {
            console.log('POST creating new project: ' + project);
            res.format({
            html: function(){
                res.location("projects");
                res.redirect("/projects");
            },
             json: function(){
                res.json(project);
            }
            });
        })
        .catch((err) => {
            res.send("There was a problem adding the information to the database: "  + err);
        })
    });

router.get('/new', function(req, res) {
    mongoose.model('User').find({})
    .then((users) => {
        res.render('projects/new', { title: 'Add New Project', "users": users });
    })
    .catch((err) => {
        res.send("Could not fetch users " + err)
    })
    
});

router.param('id', function(req, res, next, id) {
     mongoose.model('Project').findById(id)
    .then((project) => {
        req.id = id;
        next(); 
    })
    .catch((err) => {
        console.log(id + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                 },
                json: function(){
                       res.json({message : err.status  + ' ' + err});
                 }
            });
    })
});

router.route('/:id')
  .get(function(req, res) {
    mongoose.model('Project').findById(req.id)
    .then((project) => {
        console.log('GET Retrieving ID: ' + project._id);
        var pocetak = project.pocetak.toISOString();
        pocetak = pocetak.substring(0, pocetak.indexOf('T'))
        var zavrsetak = project.zavrsetak.toISOString();
        zavrsetak = zavrsetak.substring(0, zavrsetak.indexOf('T'))
        res.format({
          html: function(){
              res.render('projects/show', {
                "pocetak" : pocetak,
                "zavrsetak": zavrsetak,
                "project" : project
              });
          },
          json: function(){
              res.json(project);
          }
        });
    })
    .catch((err) => {
        console.log('GET Error: There was a problem retrieving: ' + err);
    })
  });

router.route('/:id/edit_v/:userid')
	.get(function(req, res) {
	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            console.log('GET Retrieving ID: ' + project._id);
            var pocetak = project.pocetak.toISOString();
            pocetak = pocetak.substring(0, pocetak.indexOf('T'))
            var zavrsetak = project.zavrsetak.toISOString();
            zavrsetak = zavrsetak.substring(0, zavrsetak.indexOf('T'))
	            res.format({
	                html: function(){
	                       res.render('projects/edit', {
	                          title: 'Project' + project._id,
                              "pocetak" : pocetak,
                              "zavrsetak": zavrsetak,
	                          "project" : project,
                              "userid": req.userid
	                      });
	                 },
	                json: function(){
	                       res.json(project);
	                 }
	            });
        })
        .catch((err) => {
            console.log('GET Error: There was a problem retrieving: ' + err);
        })
	})
	.put(function(req, res) {
	    var naziv = req.body.naziv;
        var opis = req.body.opis;
        var cijena = req.body.cijena;
        var pocetak = req.body.pocetak;
        var zavrsetak = req.body.zavrsetak;
        var voditelj = req.body.voditelj;
        var arhiviran = req.body.arhiviran;
        if (arhiviran == 'on'){
            arhiviran = true;
        }
        else {
            arhiviran = false;
        }

	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            project.updateOne({
	            naziv : naziv,
                opis : opis,
                cijena : cijena,
                pocetak : pocetak,
                zavrsetak: zavrsetak,
                voditelj : voditelj,
                arhiviran: arhiviran
	        })
            .then((project) =>{
                res.format({
                    html: function(){
                         res.redirect("/user/"+req.userid + "/voditelj");
                   },
                  json: function(){
                         res.json(project);
                   }
                });
            })
            .catch((err) => {
                res.send("There was a problem updating the information to the database: " + err);
            })
        })
        .catch((err) => {
            res.send("There was a problem updating the information to the database outside err: " + err);
        })
	})
	.delete(function (req, res){
	    mongoose.model('Project').findById(req.id)
        .then((project) =>{
            project.deleteOne()
            .then((project) => {
                console.log('DELETE removing ID: ' + project._id);
	                    res.format({
	                         html: function(){
	                               res.redirect("/projects");
	                         },
	                        json: function(){
	                               res.json({message : 'deleted',
	                                   item : project
	                               });
	                         }
	                      });
            })
            .catch((err) => {
                return console.error(err);
            })
        })
        .catch((err) => {
            return console.error(err);
        })
	});

    router.route('/:id/edit_v/:userid')
	.get(function(req, res) {
	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            console.log('GET Retrieving ID: ' + project._id);
            var pocetak = project.pocetak.toISOString();
            pocetak = pocetak.substring(0, pocetak.indexOf('T'))
            var zavrsetak = project.zavrsetak.toISOString();
            zavrsetak = zavrsetak.substring(0, zavrsetak.indexOf('T'))
	            res.format({
	                html: function(){
	                       res.render('projects/edit_v', {
	                          title: 'Project' + project._id,
                              "pocetak" : pocetak,
                              "zavrsetak": zavrsetak,
	                          "project" : project,
                              "userid": req.userid
	                      });
	                 },
	                json: function(){
	                       res.json(project);
	                 }
	            });
        })
        .catch((err) => {
            console.log('GET Error: There was a problem retrieving: ' + err);
        })
	})
	.put(function(req, res) {
	    var naziv = req.body.naziv;
        var opis = req.body.opis;
        var cijena = req.body.cijena;
        var pocetak = req.body.pocetak;
        var zavrsetak = req.body.zavrsetak;
        var voditelj = req.body.voditelj;
        var arhiviran = req.body.arhiviran;
        if (arhiviran == 'on'){
            arhiviran = true;
        }
        else {
            arhiviran = false;
        }

	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            project.updateOne({
	            naziv : naziv,
                opis : opis,
                cijena : cijena,
                pocetak : pocetak,
                zavrsetak: zavrsetak,
                voditelj : voditelj,
                arhiviran: arhiviran
	        })
            .then((project) =>{
                res.format({
                    html: function(){
                         res.redirect("/user/"+req.userid + "/voditelj");
                   },
                  json: function(){
                         res.json(project);
                   }
                });
            })
            .catch((err) => {
                res.send("There was a problem updating the information to the database: " + err);
            })
        })
        .catch((err) => {
            res.send("There was a problem updating the information to the database outside err: " + err);
        })
	})

router.route('/:id/edit_c/:userid')
.get(function(req, res) {
	mongoose.model('Project').findById(req.id)
    .then((project) => {
        console.log('GET Retrieving ID: ' + project._id);
        var pocetak = project.pocetak.toISOString();
        pocetak = pocetak.substring(0, pocetak.indexOf('T'))
        var zavrsetak = project.zavrsetak.toISOString();
        zavrsetak = zavrsetak.substring(0, zavrsetak.indexOf('T'))
	    res.format({
	        html: function(){
	            res.render('projects/edit_c', {
	                title: 'Project' + project._id,
                    "pocetak" : pocetak,
                    "zavrsetak": zavrsetak,
	                "project" : project,
                    "userid": req.userid
	            });
	        },
	        json: function(){
	            res.json(project);
	        }
	    });
    })
    .catch((err) => {
        console.log('GET Error: There was a problem retrieving: ' + err);        })
	});

router.route('/:id/add_clan/:userid')
	.get(function(req, res) {
	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            mongoose.model('User').find({})
            .then((users) => {
                console.log('GET Retrieving ID: ' + project._id);
	            res.format({
	                html: function(){
	                       res.render('projects/add_clan', {
	                          title : "Dodaj novog clana",
                              "project": project,
                              "users": users,
                              "userid": req.userid
	                      });
	                 },
	                json: function(){
	                       res.json(project);
	                 }
	            });
                })
            })
            .catch((err) => {
                console.log('GET Error: There was a problem retrieving: ' + err);
            })
        .catch((err) => {
            console.log('GET Error: There was a problem retrieving: ' + err);
        })
	})
	.put(function(req, res) {
	    var novi_clan = req.body.clan

	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            if( project.clanovi.includes(novi_clan)){
                res.format({
                    html: function(){
                        res.redirect("/projects/"+req.id+"/edit_v/"+req.userid);
                }
                });
            }
            else{
                project.updateOne({
                    $push: {clanovi: novi_clan}
                })
                .then((project) =>{
                    res.format({
                        html: function(){
                            res.redirect("/projects/"+req.id+"/edit_v/"+req.userid);
                    },
                    json: function(){
                            res.json(project);
                    }
                    });
                })
                .catch((err) => {
                    res.send("There was a problem updating the information to the database: " + err);
                })
            }
        })
        .catch((err) => {
            res.send("There was a problem updating the information to the database outside err: " + err);
        })
	});

router.route('/:id/remove_clan/:userid')
	.get(function(req, res) {
	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            console.log('GET Retrieving ID: ' + project._id);
	            res.format({
	                html: function(){
	                       res.render('projects/remove_clan', {
	                          title : "Obrisi postojeceg clana",
                              "project": project,
                              "userid": req.userid
	                      });
	                 },
	                json: function(){
	                       res.json(project);
	                 }
	            });
        })
        .catch((err) => {
            console.log('GET Error: There was a problem retrieving: ' + err);
        })
	})
	.put(function(req, res) {
	    var remove_clan = req.body.clan

	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            project.updateOne({
	            $pull: {clanovi: remove_clan}
	        })
            .then((project) =>{
                res.format({
                    html: function(){
                         res.redirect("/projects/"+req.id+"/edit_v/"+req.userid);
                   },
                  json: function(){
                         res.json(project);
                   }
                });
            })
            .catch((err) => {
                res.send("There was a problem updating the information to the database: " + err);
            })
        })
        .catch((err) => {
            res.send("There was a problem updating the information to the database outside err: " + err);
        })
	});

router.route('/:id/add_posao/:userid')
	.get(function(req, res) {
	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            console.log('GET Retrieving ID: ' + project._id);
	            res.format({
	                html: function(){
	                       res.render('projects/add_posao', {
	                          title : "Dodaj obavljeni posao",
                              "project": project,
                              "userid": req.userid
	                      });
	                 },
	                json: function(){
	                       res.json(project);
	                 }
	            });
        })
        .catch((err) => {
            console.log('GET Error: There was a problem retrieving: ' + err);
        })
	})
	.put(function(req, res) {
	    var novi_posao = req.body.posao;

	    mongoose.model('Project').findById(req.id)
        .then((project) => {
            project.updateOne({
	            $push: {obavljeni_poslovi: novi_posao}
	        })
            .then((project) =>{
                res.format({
                    html: function(){
                        res.redirect("/projects/"+req.id+"/edit_v/"+req.userid);
                   },
                  json: function(){
                         res.json(project);
                   }
                });
            })
            .catch((err) => {
                res.send("There was a problem updating the information to the database: " + err);
            })
        })
        .catch((err) => {
            res.send("There was a problem updating the information to the database outside err: " + err);
        })
	});

module.exports = router;