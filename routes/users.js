var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

//Any requests to this controller must pass through this 'use' function
//Copy and pasted from method-override
router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}))


router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.route('/register')
.get(function(req, res, next) {
  res.format({
    html: function(){
        res.render('users/register', {
              title: 'Register new user',
          });
        }
  });
})
.put(function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  mongoose.model('User').findOne({
    username: username
  })
  .then((user) => {
    if (user != null){
     res.format({
        html: function(){
            res.redirect("/users/register");
        }
      });
    }
    else {
      mongoose.model('User').create({
        username : username,
        password : password
      })
      .then((user) => {
        console.log('POST creating new user: ' + user);
        res.format({
        html: function(){
            res.redirect("/users/login");
        },
         json: function(){
            res.json(user);
        }
        });
      })
      .catch((err) => {
        res.send("REGISTER:There was a problem adding the information to the database: "  + err);
      })
    }
  })
  .catch((err) =>{
    res.send("REGISTER:There was a problem retreiving users from db: " + err);
  })
});

router.route('/login')
.get(function(req, res, next) {
  res.format({
    html: function(){
        res.render('users/login', {
              title: 'Login',
          });
        }
  });
})
.put(function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  mongoose.model('User').findOne({
    username : username,
    password : password
  })
  .then((user) => {
    if (user != null){
      console.log('New login from ' + user);
      res.format({
        html: function(){
          res.redirect("/users/" + user._id + "/voditelj");
      },
      json: function(){
          res.json(user);
      }
      });
    }
    else {
      res.format({
        html: function(){
            res.redirect("/users/login");
        }
      });
    }
  })
  .catch((err) => {
    res.send("LOGIN:There was a problem retreiving users from db: " + err);
  })
});

router.route('/:id/voditelj')
.get(function(req, res) {
  console.log("ID: " + req.id);
  mongoose.model('User').findById(req.id)
  .then((user) => {
    console.log('GET Retrieving user:' + user);
    var username = user.username;
    mongoose.model('Project').find({voditelj: username})
    .then((projects) => {
      console.log("Projects: " + projects);
      if(projects != null){
        res.format({
          html: function(){
              res.render('users/voditelj', {
                    title: 'Voditelj Projects',
                    "projects" : projects,
                    "userid" : req.id
                });
          },
          json: function(){
              res.json(projects);
          }
        });
      }
      else{
        res.format({
          html: function(){
              res.render('users/voditelj', {
                    title: 'No projects available',
                    "projects" : null,
                    "userid" : req.id
                });
          }
        });
      }
    })
    .catch((err) => {
      res.send("VODITELJ:There was a problem retreiving projects from db: " + err);
    })
  })
  .catch((err) => {
    res.send("VODITELJ:There was a problem retreiving users from db: " + err);
  })
});

router.route('/:id/clan')
.get(function(req, res) {
  mongoose.model('User').findById(req.id)
  .then((user) => {
    console.log('GET Retrieving user: ' + user.username);
    var username = user.username;
    mongoose.model('Project').find({clanovi: username})
    .then((projects) => {
      console.log("Projects: " + projects);
      if(projects != null){
        res.format({
          html: function(){
              res.render('users/clan', {
                    title: 'Clan Projects',
                    "projects" : projects,
                    "userid": req.id
                });
          },
          json: function(){
              res.json(projects);
          }
        });
      }
      else{
        res.format({
          html: function(){
              res.render('users/clan', {
                    title: 'No projects available',
                    "projects" : null,
                    "userid": req.id
                });
          }
        });
      }
    })
    .catch((err) => {
      res.send("CLAN:There was a problem retreiving projects from db: " + err);
    })
  })
  .catch((err) => {
    res.send("CLAN:There was a problem retreiving users from db: " + err);
  })
});

router.route('/:id/arhiva')
.get(function(req, res) {
  mongoose.model('User').findById(req.id)
  .then((user) => {
    console.log('GET Retrieving user: ' + user.username);
    var username = user.username;
    mongoose.model('Project').find({$or: [{clanovi: username} ,{voditelj: userame}]})
    .then((projects) => {
      console.log("Projects: " + projects);
      if(projects != null){
        res.format({
          html: function(){
              res.render('users/clan', {
                    title: 'Clan Projects',
                    "projects" : projects,
                    "userid": req.id
                });
          },
          json: function(){
              res.json(projects);
          }
        });
      }
      else{
        res.format({
          html: function(){
              res.render('users/clan', {
                    title: 'No projects available',
                    "projects" : null,
                    "userid": req.id
                });
          }
        });
      }
    })
    .catch((err) => {
      res.send("CLAN:There was a problem retreiving projects from db: " + err);
    })
  })
  .catch((err) => {
    res.send("CLAN:There was a problem retreiving users from db: " + err);
  })
});

module.exports = router;